package com.soportelibre;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * Empleado Service (Stateless Session Bean).
 *
 * @author Ricardo Arguello <ricardo.arguello@soportelibre.com>
 */
@Stateless
public class EmpleadoService {

    @PersistenceContext
    private EntityManager entityManager;

    private static final Logger logger = Logger.getLogger(EmpleadoService.class.getName());

    public void create(String nombre) {
        logger.info("EmpleadoService::insert()");
        Empleado empleado = new Empleado(nombre);
        entityManager.persist(empleado);
    }

    public void list() {
        logger.info("EmpleadoService::list()");
        Query query = entityManager.createQuery("SELECT e FROM Empleado e");
        List<Empleado> empleados = query.getResultList();

        for (Empleado empleado : empleados) {
            logger.info(" empleado=" + empleado);
        }
    }
}
