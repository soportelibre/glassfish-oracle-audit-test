package com.soportelibre;

import java.io.IOException;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Empleado Servlet.
 *
 * @author Ricardo Arguello <ricardo.arguello@soportelibre.com>
 */
@WebServlet("/empleado")
public class EmpleadoServlet extends HttpServlet {

    @EJB
    private EmpleadoService empleadoService;

    private static final Logger logger = Logger.getLogger(EmpleadoServlet.class.getName());
    private static final long serialVersionUID = 9036632373772327195L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("EmpleadoServlet::doGet()");
        empleadoService.create("Pedro");
        empleadoService.list();
    }
}
